var input   = document.getElementById("inputkegiatan");

var list    = document.getElementById("list");


input.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        var value   = input.value; 
        var node = document.createElement("LI");

        var att = document.createAttribute("class");       
        att.value = "list-group-item d-flex justify-content-between lh-condensed";                           
        node.setAttributeNode(att);  
        var t = document.createTextNode(value);  
        node.appendChild(t);
        list.appendChild(node);
        input.value="";
    }
});

